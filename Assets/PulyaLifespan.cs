﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulyaLifespan : MonoBehaviour
{
    private float time;

    void Start()
    {
        time = Time.realtimeSinceStartup;
    }

    private void Update()
    {
        if (Time.realtimeSinceStartup - time > 4)
        {
            Destroy(gameObject);
        }
    }
}
