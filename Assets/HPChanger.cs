﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPChanger : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private GameState gameState;

    void Update()
    {
        text.text = string.Format("HP = {0};", gameState.HP);
    }
}
