﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMover : MonoBehaviour
{
    [SerializeField] private float globalAcceleration;
    [SerializeField] private float speed;
    [SerializeField] private Animator animator;

    private CharacterController characterController;

    private Vector3 targetVelocity;
    private Vector3 velocity;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        targetVelocity = Vector3.up * globalAcceleration;
        velocity = Vector3.zero;
    }

    void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        if (Mathf.Abs(vertical) < 0.01f && Mathf.Abs(horizontal) < 0.01f)
        {
            animator.ResetTrigger("MoveStart");
            animator.SetTrigger("MoveStop");
        }
        else
        {
            animator.ResetTrigger("MoveStop");
            animator.SetTrigger("MoveStart");
        }

        Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;
        direction = transform.rotation * direction;
        targetVelocity = new Vector3(0, targetVelocity.y, 0) + direction * speed;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            velocity = new Vector3(velocity.x, 50, velocity.y);
        }

        if (characterController.isGrounded)
        {
            targetVelocity = new Vector3(targetVelocity.x, 0, targetVelocity.z);
        }
        else
        {
            targetVelocity = new Vector3(targetVelocity.x, globalAcceleration, targetVelocity.z);
        }
        velocity = Vector3.MoveTowards(velocity, targetVelocity, 1);
        characterController.Move(velocity * Time.deltaTime);
    }
}
