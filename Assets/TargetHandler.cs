﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetHandler : MonoBehaviour
{
    private EnemyMover enemyMover;

    private void Start()
    {
        enemyMover = GetComponent<EnemyMover>();
    }

    public void ShotReceive()
    {
        enemyMover.Damage(5);
    }
}
