﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMover : MonoBehaviour
{
    [SerializeField] private float globalAcceleration;
    [SerializeField] private float speed;
    [SerializeField] private float criticalRadius;
    [SerializeField] private GameState gameState;
    [SerializeField] private RectTransform HPimage;

    private CharacterController characterController;

    private Vector3 targetVelocity;
    private Vector3 velocity;

    public float MaxHP;
    public float HP;
    public float vertical = 0;
    public float horizontal = 0;
    public GameObject targetPlayer;

    int state = 0;
    public Coroutine attack;

    void Start()
    {
        HP = MaxHP;
        characterController = GetComponent<CharacterController>();
        targetVelocity = Vector3.up * globalAcceleration;
        velocity = Vector3.zero;
        Cursor.lockState = CursorLockMode.Locked;
        StartCoroutine("AttackCoroutine");
    }

    void Update()
    {
        if (targetPlayer != null)
        {
            Vector3 fromEnemyToPlayer = targetPlayer.transform.position - transform.position;
            transform.forward = fromEnemyToPlayer;
            vertical = fromEnemyToPlayer.normalized.z;
            horizontal = fromEnemyToPlayer.normalized.x;

            if (fromEnemyToPlayer.magnitude < criticalRadius)
            {
                vertical = 0;
                horizontal = 0;
            }
        }

        Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;
        targetVelocity = new Vector3(0, targetVelocity.y, 0) + direction * speed;

        if (characterController.isGrounded)
        {
            targetVelocity = new Vector3(targetVelocity.x, 0, targetVelocity.z);
        }
        else
        {
            targetVelocity = new Vector3(targetVelocity.x, globalAcceleration, targetVelocity.z);
        }
        velocity = Vector3.MoveTowards(velocity, targetVelocity, 1);
        characterController.Move(velocity * Time.deltaTime);
    }

    private IEnumerator AttackCoroutine()
    {
        while (true)
        {
            if (targetPlayer != null)
            {
                Vector3 fromEnemyToPlayer = targetPlayer.transform.position - transform.position;
                while (fromEnemyToPlayer.magnitude < criticalRadius)
                {
                    gameState.HP -= 5;
                    yield return new WaitForSeconds(2);
                    fromEnemyToPlayer = targetPlayer.transform.position - transform.position;
                }
            }
            yield return new WaitForSeconds(0.0001f);
        }
    }

    public void Damage(int damage)
    {
        HP -= damage;
        if (HP < 0)
        {
            Destroy(gameObject);
        }

        var asd = HPimage.parent.GetComponent<RectTransform>().sizeDelta;
        HPimage.sizeDelta = new Vector2(HP / MaxHP * asd.x, asd.y);
    }
}
